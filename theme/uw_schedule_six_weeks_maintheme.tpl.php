<?php

/**
 * @file
 * Page template.
 */
$image_path = drupal_get_path('module', 'uw_schedule_six_weeks'); ?>

<img src="<?php echo ($image_path); ?>/theme/first-6-weeks.jpg" alt="Get involved, get connected, get informed" />

<p>Wondering what to do now that Orientation Week is over?</p>

<p>With new friends and classes starting, you probably feel like you have lots to juggle. We’ve created a calendar just for you to help you with the transition. You can see all the events, workshops and activities available to you from services across campus, or just select the ones related to:</p>

<ul>
    <li>Health and wellness</li>
    <li>Academic success</li>
    <li>Campus involvement</li>
</ul>

<p>Don’t miss our wrap-up celebration for first-year students on Thursday, October 18th, 4:30-6:30 pm at Federation Hall. We’re celebrating the halfway point of your first term and it’s a chance to catch Andy Thibodeau again (you may have seen him at FYE: Waterloo).  Andy will talk about how your first six weeks have gone and share strategies on how to finish your term successfully. Register with your WatIAM credentials for the <a href="https://leads.uwaterloo.ca/Event/Details/83">First 6 Weeks Celebration</a>.</p>

<br />

<h2>Navigate Your First 6 Weeks</h2>

<div class="uw_orientation_schedule_wrapper">
    <form name="input" action="" method="post">
    <label for="selected_calendar">Please select a schedule:</label>
    <select id="selected_calendar" name="selected_calendar">
        <?php foreach ($calendars as $google_code => $values): ?>
        <option value='<?php print $google_code ?>' <?php print $values[1] ?>><?php print $values[0] ?></option>
        <?php endforeach; ?>
    </select> <input type="submit" value="Submit" name="submit" />
    
    </form>  
        
    <br />

    <?php if ($is_valid_post == TRUE): ?>
    <iframe src="https://www.google.com/calendar/embed?title=<?php print $calendars[$selected_calendar][0] ?> Schedule&amp;showNav=0&amp;showTabs=0&amp;showCalendars=0&amp;mode=WEEK&amp;height=800&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=<?php print $selected_calendar ?>%40group.calendar.google.com&amp;color=%23<?php print $calendars[$selected_calendar][2] ?>&amp;ctz=America%2FToronto&amp;dates=20120909%2F20120915" style=" border-width:0 " width="100%" height="800" frameborder="0" scrolling="no"></iframe>
    <?php endif; ?>
</div>

<br />

<p>We also have other services on campus to help you with your transition:</p>

<ul>
    <li><a href="https://uwaterloo.ca/writing-centre/i-need-writing-help">Writing help</a></li>
    <li><a href="https://uwaterloo.ca/student-success/learning-services/success-coaching">Success Coaching</a></li>
    <li><a href="https://uwaterloo.ca/international-students/about-international-student-office/programs/english-conversation-circles">International Student Experience</a></li>
    <li><a href="https://uwaterloo.ca/counselling-services/">Counselling Services</a></li>
</ul>
